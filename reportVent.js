const Validate = () => {
    let validate =  true
    if ($("#desde").val() == "") {
        validate = false
    }
    if ($("#hasta").val() == "") {
        validate = false
    }
   
    return validate;    
}
const Render = ()  => {
    if (Validate()) {
        let date = new Date()
        console.log("fecha",date.toISOString());
        $.ajax({
            url: `https://system-parking-bellpi.herokuapp.com/api/report/amount`,
            data: {
                "desde": $("#desde").val(),
                "hasta": $("#hasta").val()
            },
            dataType: "json",
            method: "POST",
            success: function (response) {
                console.log(response);                
                if (response.response) {
                    let data = response.data
                    let i = 1
                    $("#render").removeClass('d-none')
                    $("#render").addClass('d-block')
                    $("#table").text("")
                    data.forEach(elem => {
                        $("#table").append(`
                            <tr>
                                <th>${i++}</th>
                                <th>${elem.day}</th>
                                <th>$${elem.amount}</th>                              
                            </tr>
                        `)
                    })
                }else{
                    $("#render").removeClass('d-block')
                    $("#render").addClass('d-none')
                    $("#error").text("no hay registro para esta fecha")
                    set('error',2000)
                }  
                
                console.log(response);

            },
            statusCode: {
                404: function() {
                alert('Error, no funciona');
                }
            },
            error:function(x,xs,xt){
                window.open(JSON.stringify(x));
            }
        });   
    }else{
        $("#error").text("Escoja un rango de fechas")
        set('error',2000)
    }
    
} 
const set = (id, min) => {
    setTimeout(() => {
        $('#'+id).text('')                    
    }, min); 
}
