$(document).ready(function(){

    $(".form").submit(function(e){
        e.preventDefault();
    })

    $.ajax({
        url: `https://system-parking-bellpi.herokuapp.com/api/type/vehicle`,
        success: function (response) {
           if (response.response){
              console.log(response);
              response.data.forEach(element => {
                $("#type_vehicle").append(`
                    <option id=${element.id} value="${element.id}">${element.name}</option>   
                `)
              });
           }
        },
        statusCode: {
            404: function() {
               alert('Error, no funciona');
            }
        },
        error:function(x,xs,xt){
            window.open(JSON.stringify(x));
        }
    });   
})

const Validate = () => {
    let validate =  true
    if ($("#name").val() == "") {
        validate = false
    }
    if ($("#last_name").val() == "") {
        validate = false
    }
    if ($("#document").val() == "") {
        validate = false
    }
    if ($("#plate").val() == "") {
        validate = false
    }
    if ($("#brand").val() == "") {
        validate = false
    }
    if ($("#model").val() == "") {
        validate = false
    }
    return validate;    
}


const set = (id, min) => {
    setTimeout(() => {
        $('#'+id).text('')                    
    }, min); 
}

const Save = () => {
    if(Validate()){
        $.ajax({
            url: `https://system-parking-bellpi.herokuapp.com/api/vehicle`,
            data: {
                client: {
                    name:       $("#name").val(),
                    last_name:  $("#last_name").val(),
                    document:   $("#document").val()
                },
                vehicle: {
                    plate: $("#plate").val(),
                    brand: $("#brand").val(),
                    model: $("#model").val(),
                    id_vehicle_type: $("#type_vehicle").val()
                }
            },
            dataType: "json",
            method: "POST",
            success: function (response) {
                if(response.code == 200){
                    $("#succes").text(response.message)
                    set('succes',2000)
                    $("#parking").removeClass('d-none')
                    $("#parking").addClass('d-block')
                    $("#register").addClass('d-none')
                    RegisterParking(response.data)

                    // window.location.href =`file:///C:/xampp/htdocs/Bellpi/SystemParking/Front/home.html`;

                }else{
                    $("#error").text(response.messages)
                    set('error',2000)
                    console.log("eee");
                }
               console.log(response);
               
            },
            statusCode: {
                404: function() {
                   alert('Error, no funciona');
                }
            },
            error:function(error){
               $("#error").text("Ha ocurrido un error")
                set('error',2000) 
            }
        });
    }else{
        $("#error").text("Por favor llene todo los campos")
        set('error',2000) 
    }
}
let id_vehicle = ''

const RegisterParking = (data) => {
    console.log(data);
    id_vehicle = data.id

    let type = data.type_vehicle
    let num = 0
    let position = []
    $.ajax({
        url: `https://system-parking-bellpi.herokuapp.com/api/register`,
        success: function (response) {
            if (response.response){
                console.log("data",response.data);
                register = response.data

                $("#vehicle").val(data.brand+', '+data.plate)
                $("#type").val(data.type_vehicle)
                $("#vehicle").prop('disabled',true)
                $("#type").prop('disabled',true)

                if (type == "Motocicleta") {
                    num = 20
                }else{
                    num = 10
                }

                register.forEach(element => {
                    if (type == element.type_vehicle) {
                        position.push(element.position)
                    }
                });

                console.log("posicion",position);

                if (position.length != num) {
                    for (let index = 1; index <= num; index++) {
                        console.log("return ",find(position, index));
                        if (!find(position, index)) {
                            $("#position").append(`
                                <option id=${index} value="${index}">${type.charAt(0)} ${index}</option>   
                            `)  
                        }
                    }
                }else{
                    $("#position").append(`
                        <option>No hay posiciones disponible para ${type} </option>   
                    `) 
                }

                console.log("tipo",type);
                console.log("park",data);
            }
        }
    });  
    
}

const find = (data, num) => {
    sw = false
    data.forEach(element => {
        if (num == element) {
            sw = true
        }
    });
    return sw
}

const SaveParking = () => {
    $.ajax({
        url: `https://system-parking-bellpi.herokuapp.com/api/register`,
        data: {
            vehicle: {
                id: id_vehicle,
                position: $("#position").val()
            }
        },
        dataType: "json",
        method: "POST",
        success: function (response) {
            if(response.status == 200){
                $("#succes").text(response.messages)
                set('succes',2000)
                window.location.href =`home.html`;
            }else{
                $("#errorRegister").text(response.messages)
                set('errorRegister',2000)
            }
           console.log(response);
           
        },
        statusCode: {
            404: function() {
               alert('Error, no funciona');
            }
        },
        error:function(error){
           $("#error").text("Ha ocurrido un error")
            set('error',2000) 
        }
    });
}
