const Validate = () => {
    let validate =  true
    if ($("#desde").val() == "") {
        validate = false
    }
    if ($("#hasta").val() == "") {
        validate = false
    }
   
    return validate;    
}
const Render = ()  => {
    if (Validate()) {
        let date = new Date()
        console.log("fecha",date.toISOString());
        $.ajax({
            url: `https://system-parking-bellpi.herokuapp.com/api/report/repeat`,
            data: {
            "desde": $("#desde").val(),
            "hasta": $("#hasta").val()
            },
            dataType: "json",
            method: "POST",
            success: function (response) {
                if (response.response){
                    console.log(response);
                    $("#render").removeClass('d-none')
                    $("#render").addClass('d-block')
                    $("#position").text(response.type.charAt(0)+"-"+response.position)
                    $("#repeat").text(response.repeat+" veces")
                    $("#type").text(response.type)
                }else{
                    $("#render").removeClass('d-block')
                    $("#render").addClass('d-none')
                    $("#error").text(response.messages)
                    set('error',2000)
                }
            },
            statusCode: {
                404: function() {
                alert('Error, no funciona');
                }
            },
            error:function(x,xs,xt){
                window.open(JSON.stringify(x));
            }
        });   
    }else{
        $("#error").text("Escoja un rango de fechas")
        set('error',2000)
    }
    
} 
const set = (id, min) => {
    setTimeout(() => {
        $('#'+id).text('')                    
    }, min); 
}
