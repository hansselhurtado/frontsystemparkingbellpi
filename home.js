$(document).ready(function(){
    Render()   
})

const Render = ()  => {
    let a = 10;//autos
    let m = 20;//motos
    let b = 10;//bicicleta
    $.ajax({
        url: `https://system-parking-bellpi.herokuapp.com/api/register`,
        success: function (response) {
            if (response.response){
                console.log(response);
                Paint('A','autos',a)
                Paint('M','moto', m)
                Paint('B','bici', b)
                Travel(response.data)

            }
        },
        statusCode: {
            404: function() {
               alert('Error, no funciona');
            }
        },
        error:function(x,xs,xt){
            window.open(JSON.stringify(x));
        }
    });   
}  

const Paint = (type, name, line) => {
    let num = 1 + line/2
    let id  = 1 + line/2
    if (name == "bici") {
        for (let index = 1; index <= line; index++) {
            $("#" + name + "1").append(`
                <td class="bg-primary" id="${type}-${index}">${type}-${index}</td>   
            `)
        }
    }else if (name == "moto") {
        for (let index = 1; index <= line/2; index++) {
            $("#" + name + "1").append(`
                <td class="bg-primary" id="${type}-${id ++ }">${type}-${num ++ }</td>   
            `)
            $("#" + name + "2").append(`
                <td class="bg-primary" id="${type}-${index}">${type}-${index}</td>   
            `)
           
           
        }
    }else{
        for (let index = 1; index <= line/2; index++) {
            
            $("#" + name + "1").append(`
                <td class="bg-primary" id="${type}-${id ++ }">${type}-${num ++ }</td>   
            `)
            $("#" + name + "2").append(`
                <td class="bg-primary" id="${type}-${index}">${type}-${index}</td>   
            `)
        }
    }
}

let id_register = ''
let id_click = ''

const Travel = (data) => {
    console.log("travel",data);
    data.forEach(element => {
        $("#"+element.type_vehicle.charAt(0)+'-'+element.position).addClass("bg-danger")
        $("#"+element.type_vehicle.charAt(0)+'-'+element.position).prop("title",element.name+', '+element.document)
        $("#"+element.type_vehicle.charAt(0)+'-'+element.position).text(element.type_vehicle.charAt(0)+'-'+element.position+" "+element.plate)
        $("#"+element.type_vehicle.charAt(0)+'-'+element.position).click(function(){
            if (id_click) {
                $("#"+id_click).removeClass("bg-success")  
                $("#"+id_click).addClass("bg-danger")  
                $("#"+element.type_vehicle.charAt(0)+'-'+element.position).removeClass("bg-danger")  
                $("#"+element.type_vehicle.charAt(0)+'-'+element.position).addClass("bg-success")
            }else{
                $("#"+element.type_vehicle.charAt(0)+'-'+element.position).removeClass("bg-danger")  
                $("#"+element.type_vehicle.charAt(0)+'-'+element.position).addClass("bg-success")  
            }
            id_click    = $("#"+element.type_vehicle.charAt(0)+'-'+element.position).attr('id')
            id_register = element.id
            console.log("click",id_click);
        })
    });
}

const Abandonar = () => {
    if (id_register) {
        console.log("abandonar", id_register);
        $.ajax({
            url: `https://system-parking-bellpi.herokuapp.com/api/register/`+ id_register,
            dataType: "json",
            method: "PUT",
            success: function (response) {
                if (response.code == 200){
                    console.log("response",response);
                    data = response.data
                    if (data.discount) {
                        amount = data.amount.replace(/[,]/g,'')
                        discount = parseInt(amount) + (parseInt(amount) *(data.discount_percentage/100))
                        console.log("sin coma",amount);
                    }
                    let html = `<div class="d-flex flex-column container w-100">
                                    <div  class="d-flex justify-content-between">
                                        <strong>Cliente</strong>
                                        <span>${data.name}</span>
                                    </div>
                                    <div  class="d-flex justify-content-between">
                                        <strong>Documento</strong>
                                        <span>${data.document}</span>
                                    </div>
                                    <div  class="d-flex justify-content-between">
                                        <strong>Vehiculo</strong>
                                        <span>${data.brand} ${data.model}</span>
                                    </div>
                                    <div  class="d-flex justify-content-between">
                                        <strong>Placa</strong>
                                        <span>${data.plate}</span>
                                    </div>
                                    <div  class="d-flex justify-content-between">
                                        <strong>Tipo de vehículo</strong>
                                        <span>${data.type_vehicle}</span>
                                    </div>
                                    <div  class="d-flex justify-content-between">
                                        <strong>Posición</strong>
                                        <span>${data.type_vehicle.charAt(0)}-${data.position}</span>
                                    </div>
                                    ${data.discount ? `
                                                    <div  class="d-flex justify-content-between">
                                                        <strong>Subtotal</strong>
                                                        <span>${formatter.format(discount)}</span>
                                                    </div>
                                                    <div  class="d-flex justify-content-between">
                                                        <strong>Descuento</strong>
                                                        <span>${data.discount_percentage}%</span>
                                                    </div>
                                                    <div  class="d-flex justify-content-between">
                                                        <strong>Total</strong>
                                                        <span>$${data.amount}</span>
                                                    </div>
                                                        `:`
                                                            <div  class="d-flex justify-content-between">
                                                                <strong>Total</strong>
                                                                <span>$${data.amount}</span>
                                                            </div>
                                                        ` }
                                    <div  class="d-flex justify-content-between">
                                        <strong>Permanencia</strong>
                                        <span>${data.duration}</span>
                                    </div>
                                    <div  class="d-flex justify-content-between">
                                        <strong>Fecha de entrada</strong>
                                        <span>${data.admission}</span>
                                    </div>
                                    <div  class="d-flex justify-content-between">
                                        <strong>Fecha de salida</strong>
                                        <span>${data.exit}</span>
                                    </div>
                                </div>`

                    Swal.fire({
                        title: "<i>Vehículo fuera del parqueadero</i>", 
                        text: 'Resumen de factura',
                        html: html, 
                        icon: "success",
                        confirmButtonText: "Aceptar", 
                    })
                    .then(() => {
                       location.reload();
                    });
                   
                }else{
                    $("#error").text(response.message)
                    set('error',2000) 
                }
            }
        });  
        
    }else{
        $("#error").text("Debe seleccionar una posicion del parqueadero")
        set('error',2000) 
    }
}

const set = (id, min) => {
    setTimeout(() => {
        $('#'+id).text('')                    
    }, min); 
}

// formatear los precios
const formatter = new Intl.NumberFormat('es-CO', {
    style: "currency",
    currency: 'COP',
    minimumFractionDigits: 0
});



const Validate = () => {
    let validate =  true
    if ($("#name").val() == "") {
        validate = false
    }
    if ($("#discount").val() == "") {
        validate = false
    }
    if ($("#minutes").val() == "") {
        validate = false
    }
    return validate;    
}

const saveDiscount = () => {
    if (Validate()) {
        $.ajax({
            url: `https://system-parking-bellpi.herokuapp.com/api/discount`,
            data: {
                discount: {
                    name:      $("#name").val(),
                    discount:  $("#discount").val(),
                    minutes:   $("#minutes").val()
                },
            },
            dataType: "json",
            method: "POST",
            success: function (response) {
                if(response.status == 200){
                    location.reload();    
                }else{
                    $("#errorDiscount").text(response.messages)
                    set('errorDiscount',2000)
                }
               console.log(response);
               
            },
            statusCode: {
                404: function() {
                   alert('Error, no funciona');
                }
            },
            error:function(error){
               $("#error").text("Ha ocurrido un error")
                set('error',2000) 
            }
        });
    }else{
        $("#errorDiscount").text("Faltan campos por llenar")
        set('errorDiscount',2000) 
    }
    
}

const Parking = () => {
    console.log("ho");
    $.ajax({
        url: `https://system-parking-bellpi.herokuapp.com/api/vehicle`,
        success: function (response) {
            console.log("jola");
            console.log(response);
            if (response.response) {
                $("#vehicle").html("")
                $("#position").html("")
                let data = response.data
                console.log("jola");
                console.log(data);
                $("#vehicle").append(`
                    <option value="" selected disabled>Seleccionar</option>      
                `)
                $("#position").append(`
                    <option value="" selected disabled>Seleccionar</option>      
                `)
                data.forEach(element => {                   
                    $("#vehicle").append(`
                        <option id="${element.type_vehicle}-${element.id}"  value="${element.id}">${element.model}, placa(${element.plate}) ${element.type_vehicle}, ${element.client}</option>   
                    `)
                })
            }
        },
        statusCode: {
            404: function() {
               alert('Error, no funciona');
            }
        },
        error:function(x,xs,xt){
            window.open(JSON.stringify(x));
        }
    }); 
}

let id_vehicle = ''
const handlePosition = () => {

    let id = $('#vehicle').val()
    id_vehicle = id
    let type = $('#vehicle option:selected').attr('id')
    let position = []
    console.log($('#vehicle').val());
    console.log($('#vehicle option:selected').attr('id'));

    $.ajax({
        url: `https://system-parking-bellpi.herokuapp.com/api/register`,
        success: function (response) {
            if (response.response){
                console.log("data",response.data);
                register = response.data

                if (type == "Motocicleta-"+id) {
                    num = 20
                }else{
                    num = 10
                }

                register.forEach(element => {
                    if (type == element.type_vehicle+"-"+id) {
                        position.push(element.position)
                    }
                });

                console.log("posicion",position);
                $("#position").html("")
                $("#position").append(`
                    <option value="" selected disabled>Seleccionar</option>      
  
                `) 
                if (position.length != num) {
                    for (let index = 1; index <= num; index++) {
                        console.log("return ",find(position, index));
                        if (!find(position, index)) {
                            $("#position").append(`
                                <option id=${index} value="${index}">${type.charAt(0)} ${index}</option>   
                            `)  
                        }
                    }
                }else{
                    $("#position").append(`
                        <option>No hay posiciones disponible para ${type} </option>   
                    `) 
                }

                console.log("tipo",type);
            }
        }
    });  
}
const SaveParking = () => {
    if (ValidateVehicle) {
        $.ajax({
            url: `https://system-parking-bellpi.herokuapp.com/api/register`,
            data: {
                vehicle: {
                    id: id_vehicle,
                    position: $("#position").val()
                }
            },
            dataType: "json",
            method: "POST",
            success: function (response) {
                if(response.status == 200){
                    location.reload();
                }else{
                    $("#errorVehicle").text(response.messages)
                    set('errorVehicle',2000)
                }
               console.log(response);
               
            },
            statusCode: {
                404: function() {
                   alert('Error, no funciona');
                }
            },
            error:function(error){
               $("#errorVehicle").text("Ha ocurrido un error")
                set('errorVehicle',2000) 
            }
        });
    }else{
        $("#errorVehicle").text("Seleccione un vehiculo")
        set('errorVehicle',2000)
    }    
}

const ValidateVehicle = () => {
    let validate =  true
    if ($("#position").val() == "") {
        validate = false
    }
   
    return validate;    
}

const find = (data, num) => {
    sw = false
    data.forEach(element => {
        if (num == element) {
            sw = true
        }
    });
    return sw
}