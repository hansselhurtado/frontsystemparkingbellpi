$(document).ready(function(){
    Render()    
})


const Render = ()  => {
    $.ajax({
        url: `https://system-parking-bellpi.herokuapp.com/api/vehicle`,
        success: function (response) {
            if (response.response) {
                let data = response.data
                let i = 1
                data.forEach(elem => {
                    $("#table").append(`
                        <tr>
                            <th>${i++}</th>
                            <th>${elem.brand} ${elem.model}</th>
                            <th>${elem.plate}</th>                              
                            <th>${elem.type_vehicle}</th>                              
                            <th>${elem.client}</th>                              
                            <th>${elem.docmuent}</th>                              
                            <th>${elem.date}</th>                              
                            <th>${elem.status}</th>                              
                        </tr>
                    `)
                })
            }
        },
        statusCode: {
            404: function() {
               alert('Error, no funciona');
            }
        },
        error:function(x,xs,xt){
            window.open(JSON.stringify(x));
        }
    });   
} 
