$(document).ready(function(){

    $.ajax({
        url: `https://system-parking-bellpi.herokuapp.com/api/type/vehicle`,
        success: function (response) {

            console.log(response);
            if (response.response) {
                let data = response.data
                let i = 1
                data.forEach(elem => {
                    $("#table").append(`
                        <tr>
                            <th>${i++}</th>
                            <th> ${elem.name}</th>
                            <th>$${elem.rate}</th>                              
                            <th>${elem.status == "1" ? 'Activo':'Desactivado'}</th>                              
                            <th>
                                <a class="dropdown-item"  data-toggle="modal" data-target="#modal" onclick="Render('${elem.id}','${elem.name}','${elem.rate}','${elem.status}')" href="#">Editar</a>
                            </th>                              
                        </tr>
                    `)
                })
            }
        },
        statusCode: {
            404: function() {
               alert('Error, no funciona');
            }
        },
        error:function(x,xs,xt){
            window.open(JSON.stringify(x));
        }
    });   
})




const Fresh = () => {
    $("#name").val("")
    $("#rate").val("")
    $("#id").val("")
    $("#state").html("")
}

const Render = (id,name,rate,status)  => {
    Fresh()
    console.log("hola",name);
    $("#name").val(name)
    $("#rate").val(rate)
    $("#id").val(id)
    if (status == '1') {
        $("#state").append(`
            <option value="${status}">Activo</option>   
            <option value="0">Desactivado</option>   
        `)
    }else{
        $("#state").append(`
            <option value="${status}">Desactivado</option>   
            <option value="1">Activo</option>   
        `)
    }
} 

const Validate = () => {
    let validate =  true
    if ($("#name").val() == "") {
        validate = false
    }
    if ($("#rate").val() == "" || $("#rate").val() == 0) {
        validate = false
    }
    if ($("#state").val() == "") {
        validate = false
    }
   
    return validate;    
}


const Save = ()  => {
    let id = $("#id").val()
    if (Validate()) {
        let date = new Date()
        console.log("fecha",date.toISOString());
        $.ajax({
            url: `https://system-parking-bellpi.herokuapp.com/api/handle/vehicle/`+id,
            data: {
                "name": $("#name").val(),
                "rate": $("#rate").val(),
                "state": $("#state").val()
            },
            dataType: "json",
            method: "POST",
            success: function (response) {
                console.log(response);                
                if (response.response) {
                    location.reload();                    
                }else{
                    console.log(response);
                }  
            },
            statusCode: {
                404: function() {
                alert('Error, no funciona');
                }
            },
            error:function(x,xs,xt){
                window.open(JSON.stringify(x));
            }
        });   
    }else{
        console.log("error");
        $("#error").text("Llene todos los campos")
        set('error',2000)
    }
    
} 
const set = (id, min) => {
    setTimeout(() => {
        $('#'+id).text('')                    
    }, min); 
}
