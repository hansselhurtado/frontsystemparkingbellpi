
$(document).ready(function(){

    $(".form").submit(function(e){
        e.preventDefault();
    })

    $.ajax({
        url: `https://system-parking-bellpi.herokuapp.com/api/type/vehicle`,
        success: function (response) {
           if (response.response){
              console.log(response);
              response.data.forEach(element => {
                $("#type_vehicle").append(`
                    <option value="${element.id}">${element.name}</option>   
                `)
              });
           }
        },
        statusCode: {
            404: function() {
               alert('Error, no funciona');
            }
        },
        error:function(x,xs,xt){
            window.open(JSON.stringify(x));
        }
    });   
})


const Validate = () => {
    let validate =  true
    if ($("#desde").val() == "") {
        validate = false
    }
    if ($("#hasta").val() == "") {
        validate = false
    }
   
    return validate;    
}
const Render = ()  => {
    if (Validate()) {
        let date = new Date()
        console.log("fecha",date.toISOString());
        $.ajax({
            url: `https://system-parking-bellpi.herokuapp.com/api/report/vehicle`,
            data: {
                "desde": $("#desde").val(),
                "hasta": $("#hasta").val(),
                "tipo":  $("#type_vehicle").val(),
            },
            dataType: "json",
            method: "POST",
            success: function (response) {
                console.log("resp",response);
                data = response.data
                let i = 1
                if (data.length > 0) {
                    $("#render").removeClass('d-none')
                    $("#render").addClass('d-block')
                    $("#table").text("")
                    data.forEach(elem => {
                        $("#table").append(`
                            <tr>
                                <th>${i++}</th>
                                <th>${elem.name}</th>
                                <th>${elem.brand} ${elem.model}</th>
                                <th>${elem.plate}</th>
                                <th>${elem.type_vehicle}</th>
                                <th>${elem.type_vehicle.charAt(0)}-${elem.position}</th>
                                <th>${elem.duration}</th>                                
                                <th>$${elem.amount}</th>                                
                                <th>${elem.discount_percentage ? `${elem.discount_percentage}%`:`No se le aplico ningún descuento`}</th>                                
                                <th>$${elem.rate}</th>                                
                                <th>${elem.admission}</th>                                
                                <th>${elem.exit}</th>                                
                            </tr>
                        `)
                    })
                }else{
                    $("#render").removeClass('d-block')
                    $("#render").addClass('d-none')
                    $("#error").text("no hay registro para esta fecha")
                    set('error',2000)
                }  
                
                console.log(response);

            },
            statusCode: {
                404: function() {
                alert('Error, no funciona');
                }
            },
            error:function(x,xs,xt){
                window.open(JSON.stringify(x));
            }
        });   
    }else{
        $("#error").text("Escoja un rango de fechas")
        set('error',2000)
    }
    
} 
const set = (id, min) => {
    setTimeout(() => {
        $('#'+id).text('')                    
    }, min); 
}
