$(document).ready(function(){
    Render()    
})

const Render = ()  => {
    $.ajax({
        url: `https://system-parking-bellpi.herokuapp.com/api/discount`,
        success: function (response) {
            if (response.response) {
                let data = response.data
                let i = 1
                data.forEach(elem => {
                    $("#table").append(`
                        <tr>
                            <th>${i++}</th>
                            <th>${elem.name}</th>
                            <th>${elem.minutes}</th>                              
                            <th>${elem.discount}</th>                              
                            <th>${elem.status}</th>                                                        
                            <th>
                                <button type="submit" class="btn btn-primary" id="abandonar" onclick="Handle('${elem.id}','${elem.status}')">
                                     <span aria-hidden="true">${elem.status == 'Activo' ? 'Desactivar':'Activar'}</span>
                                </button>
                            </th>                                                        
                        </tr>
                    `)
                })
            }
        },
        statusCode: {
            404: function() {
               alert('Error, no funciona');
            }
        },
        error:function(x,xs,xt){
            window.open(JSON.stringify(x));
        }
    });   
} 

const Handle = (id, state)  => {
   
    $.ajax({
        url: `https://system-parking-bellpi.herokuapp.com/api/handle/discount/`+id,
        data: {
            "state": state == 'Activo' ? 0: 1
        },
        dataType: "json",
        method: "POST",
        success: function (response) {
            console.log(response);                
            if (response.response) {
                location.reload();
            }  
        },
        statusCode: {
            404: function() {
            alert('Error, no funciona');
            }
        },
        error:function(x,xs,xt){
            window.open(JSON.stringify(x));
        }
    });   
    
} 
